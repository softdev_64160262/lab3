/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chachai.lab3;

/**
 *
 * @author Lenovo
 */
class OXProgram {

    public static boolean checkRows(String[][] board, int row, int col, String turn) {
        for (int j = 0; j < 3; j++) {
            if (!board[row - 1][j].equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkColumns(String[][] board, int row, int col, String turn) {
        for (int j = 0; j < 3; j++) {
            if (!board[0][col - 1].equals(turn)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDiagonals(String[][] board, int row, int col, String turn) {

        if (row - 1 == col - 1) {
            for (int j = 0; j < 3; j++) {
                if (!board[j][j].equals(turn)) {
                    return false;
                }

            }
            return true;
        }
        if ((row + col) - 2 == 2) {
            for (int j = 0; j < 3; j++) {
                if (!board[j][2 - j].equals(turn)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean checkDraw(String[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j].equals("_")) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkWin(String[][] board, int row, int col, String turn) {
        if (checkRows(board, row, col, turn) || checkColumns(board, row, col, turn) || checkDiagonals(board, row, col, turn)) {
            return true;
        }

        return false;
    }
}
