/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.chachai.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lenovo
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoplayBY_X_output_false() {
        String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        String turn = "X";
        int row = 1;
        int col = 1;
        assertEquals(false, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Rows_BY_X_output_true() {
        String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"X", "X", "X"}};
        String turn = "X";
        int row = 3;
        int col = 3;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Rows_BY_O_output_true() {
        String[][] board = {{"O", "O", "O"}, {"_", "_", "_"}, {"X", "X", "_"}};
        String turn = "O";
        int row = 1;
        int col = 1;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Col_BY_X_output_true() {
        String[][] board = {{"X", "_", "_"}, {"X", "_", "_"}, {"X", "_", "_"}};
        String turn = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Col_BY_O_output_true() {
        String[][] board = {{"X", "_", "O"}, {"_", "_", "O"}, {"X", "_", "O"}};
        String turn = "O";
        int row = 3;
        int col = 3;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Diagonal_BY_X_output_true() {
        String[][] board = {{"X", "_", "_"}, {"_", "X", "O"}, {"O", "_", "X"}};
        String turn = "X";
        int row = 1;
        int col = 1;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckWin_Diagonal_BY_O_output_true() {
        String[][] board = {{"_", "_", "O"}, {"_", "O", "_"}, {"O", "_", "_"}};
        String turn = "O";
        int row = 3;
        int col = 1;
        assertEquals(true, OXProgram.checkWin(board, row, col, turn));
    }

    @Test
    public void testCheckDraw() {
        String[][] board = {{"O", "X", " O"}, {"O", "X", "O"}, {"X", "O", "X"}};
        assertEquals(true, OXProgram.checkDraw(board));
    }

}
